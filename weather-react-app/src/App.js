import logo from './logo.svg';
import './App.css';
import React from 'react';
import WeatherPerWeek from './components/WeatherPerWeek';
import MapWeatherForecast from './components/MapWeatherForecast';
import LocationPhoto from './components/LocationPhoto';
import {
BrowserRouter as Router,
Switch,
Route,
Link,
Redirect,
useLocation
} from "react-router-dom";
import moment from 'moment'


function App() {
return (
<Router>
<div>
<nav>
<ul class="nav">
<li>
<Link to="/">Home</Link>
</li>
<li>
<Link to="/weatherperweek">Weather per week</Link>
</li>
<li>
<Link to="/location">Location Photo</Link>
</li>
<li>
<Link to="/weathermap">Maps of weather</Link>
</li>

</ul>
</nav>

<div className="App">
<h1>Weather Forecast</h1>
<section>
<Switch>
<Route path="/weatherperweek/:id">
<WeatherPerWeek />

</Route>
<Route path="/weatherperweek">
<Redirect to={`/weatherperweek/${moment().format('YYYY-MM-DD')}`} />
</Route>
<Route path="/location">
<LocationPhoto />
</Route>
<Route path="/weathermap">
<WeatherMaps />
</Route>

<Route path="/">
<h1>Home</h1>
Welcome to our service. You can explore <Link to="/weatherperweek">weather per week. </Link> <br>
</br>
Also you can find <Link to="/location">Photo of place</Link> Which upgrade every 5 minutes.
<br></br>
If you want to find weather in your city <Link to="/weathermap">Please, click hear!</Link>
</Route>

<Route path="*">
<NoMatch />
</Route>

</Switch>

<br></br><br></br><p>by Project Pentium©</p>
</section>
</div>
</div>
</Router>
);
}

function NoMatch() {
let location = useLocation();

return (
<div>
<h3>
No match for <code>{location.pathname}</code>
</h3>
</div>
);
}

export default App;
import React from 'react';
import * as Api from 'typescript-fetch-api'
import { withRouter } from "react-router";
import moment from 'moment'
import Moment from 'react-moment';



class Location extends React.Component {

    constructor(props) {
        super(props);
        const id = this.props.match?.params.id || moment().format('YYYY-MM-DD');
        console.log(id);



        this.state = {
            events: [{id:'id123', date: "2021-04-21", weather: "Sunny", img:"Mountain"},
            {id:'id13', date: "2021-04-21", weather: "Rainy", img :"Park"},
            {id:'id13', date: "2021-04-21", weather: "Cold, snow", img:"City"}
            ],
            date: id
        };


        this.handleReload = this.handleReload.bind(this);
        this.handleReload();
    }


    async handleReload(event) {
    //const response = await api.events({ date: '2021-03-25'/*this.state.targetDate*/ });
    //this.setState({ events: response });
    }


    render() {
        return <div>
        {/*<button onClick={this.handleReload}>Reload</button> */}
        <h2>Places</h2>
        <h3>You can see photos on <Moment format="YYYY/MM/DD">{this.state.date}</Moment> in Different places </h3>
        <ul>
        {this.state.events.map(
        (photo) =>
        <li key={photo.id}><Moment format="YYYY/MM/DD">{this.state.date}</Moment>: {photo.weather} is in {photo.img}</li>)}
        </ul>
        </div>
        }
}

export default withRouter(Location);